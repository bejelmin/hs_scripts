#!/bin/bash

# script to set up the VM to use the HEPscore BMK suite - on CentoS8 & Alma9

ENDCOLOR="\e[0m"
GREEN="\e[32m"
CYAN="\e[36m"
YELLOW="\e[33m"

# initial commands
run_initial_commands(){

    echo -e "${CYAN}Running initial commands${ENDCOLOR}"
    cd /etc/yum.repos.d/

    # TODO: do only for centOS, not other OS
    sudo sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-*
    sudo sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-*
    sudo yum update -y

    # when done, go back to home
    cd
    echo -e "${GREEN}Initial commands: done!${ENDCOLOR}"

}

# bash setups
set_bash_env(){

    # go to home
    cd 

    echo -e "${CYAN}\nSetting up .bashrc, .bash_aliases, and .bash_profile${ENDCOLOR}"
    
    cat >> .bashrc << EOF
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi
EOF

    cat >> .bash_profile << EOF
export PS1="\[\033[35;1m\]\u\[\033[m\]@\[\033[34;1m\]\h:\[\033[33;1m\]\w\[\033[m\]\$ "
export CLICOLOR=1
export LSCOLORS=ExFxBxDxCxegedabagacad
EOF

    cat > .bash_aliases << EOF
# .bash_aliases

alias cp='cp -iv'
alias mv='mv -iv'
alias rm='rm -v'
alias mkdir='mkdir -pv'

if [ "\$TERM" != "dumb" ] && [ -x /usr/bin/dircolors ]; then
        test -r ~/.dircolors && eval "\$(dircolors -b ~/.dircolors)" || eval "\$(dircolors -b)"
                alias ll='ls -FGlAhp --color=auto'
                alias dir='dir --color=auto'
                alias vdir='vdir --color=auto'
                alias grep='grep --color=auto'
                alias fgrep='fgrep --color=auto'
                alias egrep='egrep --color=auto'
fi
EOF

    echo -e "Sourcing bash changes"
    source .bash_profile
    echo -e "${GREEN}Setting up: done :)${ENDCOLOR}"

    # vim setup
    echo -e "${CYAN}\nSetting up vim configuration${ENDCOLOR}"
    cat >> .vimrc << EOF
set ruler
set showcmd

colorscheme default

set number
syntax on
filetype indent on

set cursorline
set autoindent
EOF

    echo -e "${GREEN}vim setup: done!${ENDCOLOR}"
    
}

# install apptainer
install_apptainer(){

    echo -e "${CYAN}\nStart: install apptainer${ENDCOLOR}"
    sudo yum install -y epel-release
    sudo yum install -y apptainer
    echo -e "Apptainer version: $( apptainer version )"
    echo -e "${GREEN}Done: apptainer installed!${ENDCOLOR}"

}

# install python
install_python(){

    VERSION=$1

    echo -e "${CYAN}\nStart: install python ${VERSION}${ENDCOLOR}"
    sudo yum upgrade -y
    sudo yum groupinstall 'development tools' # requires input
    sudo yum install wget openssl-devel bzip2-devel libffi-devel #requires input
    cd ~
    wget https://www.python.org/ftp/python/${VERSION}/Python-${VERSION}.tgz
    tar xzf Python-${VERSION}.tgz
    cd Python-${VERSION}
    ./configure --enable-optimizations
    sudo make
    sudo make install
    cd ~
    pip3 install --upgrade pip setuptools

    # check python and pip version
    echo -e "Python version: $( python3 --version )"
    echo -e "pip version: $( pip3 --version )"

    echo -e "${GREEN}Install python: done!${ENDCOLOR}"

}

echo -e "${GREEN}Set up the virtual machine to run hepscore suite!${ENDCOLOR}"

run_initial_commands

if [ -f ".bash_aliases" ]; then
    echo -e "${YELLOW}\nBash env already set, skipping...${ENDCOLOR}"
else
    set_bash_env
fi

if ! command -v apptainer &> /dev/null ; then
    install_apptainer
else
    echo -e "${YELLOW}\nApptainer already installed, skipping...${ENDCOLOR}"
fi

CMD="vim"
if ! command -v ${CMD} &> /dev/null ; then
    echo -e "${CYAN}\nStart: install ${CMD}${ENDCOLOR}"
    sudo yum install ${CMD} # requires input
    echo -e "${GREEN}Install ${CMD}: done!${ENDCOLOR}"
else
    echo -e "${YELLOW}\n${CMD} already installed, skipping...${ENDCOLOR}"
fi

CMD="screen"
if ! command -v ${CMD} &> /dev/null ; then
    echo -e "${CYAN}\nStart: install ${CMD}${ENDCOLOR}"
    sudo yum install ${CMD} # requires input
    echo -e "${GREEN}Install ${CMD}: done!${ENDCOLOR}"
else
    echo -e "${YELLOW}\n${CMD} already installed, skipping...${ENDCOLOR}"
fi

CMD="bc"
if ! command -v ${CMD} &> /dev/null ; then
    echo -e "${CYAN}\nStart: install ${CMD}${ENDCOLOR}"
    sudo yum install ${CMD} # requires input
    echo -e "${GREEN}Install ${CMD}: done!${ENDCOLOR}"
else
    echo -e "${YELLOW}\n${CMD} already installed, skipping...${ENDCOLOR}"
fi

VER="3.9.19"
if ! command -v python3 &> /dev/null ; then
    install_python ${VER}
else
    echo -e "${YELLOW}\nPython ${VER} already installed, skipping...${ENDCOLOR}"
fi

CMD="git"
if ! command -v ${CMD} &> /dev/null ; then
    echo -e "${CYAN}\nStart: install ${CMD}${ENDCOLOR}"
    sudo yum install ${CMD} # requires input
    echo -e "${GREEN}Install ${CMD}: done!${ENDCOLOR}"
else
    echo -e "${YELLOW}\n${CMD} already installed, skipping...${ENDCOLOR}"
fi

CMD="htop"
if ! command -v ${CMD} &> /dev/null ; then
    echo -e "${CYAN}\nStart: install ${CMD}${ENDCOLOR}"
    sudo yum install ${CMD} # requires input
    echo -e "${GREEN}Install ${CMD}: done!${ENDCOLOR}"
else
    echo -e "${YELLOW}\n${CMD} already installed, skipping...${ENDCOLOR}"
fi

echo -e "${GREEN}\nSetup of VM is done! Now you can run the HEPscore suite!"
echo -e "Remember to mount the volume!"
echo -e "Enjoy! :)${ENDCOLOR}"

