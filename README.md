# hs_scripts

Current version of suite scripts: 3.0rc11

**Note**: the script version has to match the suite version! 

## How to use

Log into the VM as a privileged user.

1. First, install git:
    ```bash
    sudo yum install git
    ```

2. Then, clone this repo and launch the script to setup the VM:
    ```bash
    git clone https://gitlab.cern.ch/bejelmin/hs_scripts.git
    cd hs_scripts
    yes | source setup_script.sh
    source ~/.bash_profile
    ```

3. If there is less than 40 GB of disk on the VM, mount an external volume, and make it writable by all users. Follow instructions below.

4. Finally, run the short test to check if everything is working properly:
    ```bash
    mkdir test
    cd test
    cp ~/hs_scripts/launch_test.sh .
    cp ~/hs_scripts/run_HEPscore_short_test.sh .
    source launch_test.sh
    ```
    You can run the test with `screen`.
    **Remember**: if you mounted a volume, run the test in the volume.

5. If no error is returned, you are ready to run the whole version of the suite; check scripts `run_HEPscore_rc11.sh` and `run_HEPscore_configurable_ncores_rc11.sh`.
    
## Instructions to mount a volume

After setting up the VM, remember to add an additional volume (at least 40 GiB), otherwise it is not possible to run hepscore 2023.

1. Create a volume from the dashboard

2. Attach the volume to an instance from the dashboard

3. Log into the instance

4. Check the disk: `grep vdb /proc/partitions`

5. Create file system (only the first time you create a new volume!): `sudo mkfs -t ext4 -L <label> /dev/vdb`

6. Mount the volume on `/mnt`: `sudo mount -L <label> /mnt` or `sudo mount /dev/vdb /mnt`

7. Make `/mnt` readable and writable by all users: `sudo chmod 777 /mnt`

8. Check: `df -h`

9. Done! Enjoy :)

Detach a volume:
```bash
umount -l /mnt
```

## Description

Collection of useful scripts related to HEPscore suite.

- `setup_script.sh`: set up a vergin VM; it works on CentOS8 and Alma9; it installs apptainer, git, python3, vim, screen, htop, bc; run on terminal as 
    ```bash
    yes | source setup_script.sh
    ```

- `run_HEPscore_short_test.sh`: run the 3.0rc11 version of the suite; only ATLAS gen workload with 1 repetition. Easy way to check if everything is working.

- `launch_test.sh`: clone the repo and launch `run_HEPscore_short_test.sh`